use day25::*;
use std::fs;

fn main() {
    let input = fs::read_to_string("inputs/day25.txt").unwrap();
    let input = parser::code(&input).unwrap();
    println!("{input:?}");
    println!("{}", Cpu::a_for_cycle(input));
}
