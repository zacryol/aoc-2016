use pathfinding::prelude::astar;
use std::collections::HashSet;

type Point = (u16, u16);

fn is_wall((x, y): Point, num: u16) -> bool {
    ((x * x + 3 * x + 2 * x * y + y + y * y) + num).count_ones() % 2 == 1
}

fn point_successors(p: Point, num: u16) -> impl Iterator<Item = Point> {
    [
        (p.0.saturating_sub(1), p.1),
        (p.0.saturating_add(1), p.1),
        (p.0, p.1.saturating_add(1)),
        (p.0, p.1.saturating_sub(1)),
    ]
    .into_iter()
    .filter(move |p| !is_wall(*p, num))
}

pub fn calc_path(num: u16) -> Option<(Vec<(u16, u16)>, u16)> {
    const END: Point = (31, 39);
    astar(
        &(1u16, 1u16),
        |p| point_successors(*p, num).map(|p| (p, 1)),
        |p| p.0.abs_diff(END.0) + p.1.abs_diff(END.1),
        |p| *p == END,
    )
}

pub fn flood_fill(num: u16, dist: u16) -> HashSet<(u16, u16)> {
    (0..dist).fold(HashSet::from([(1, 1)]), |hs: HashSet<_>, _| {
        hs.iter()
            .flat_map(|&p| point_successors(p, num).chain([p]))
            .collect::<HashSet<_>>()
    })
}
