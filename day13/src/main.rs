use day13::*;
use std::fs;

fn main() {
    let input = fs::read_to_string("inputs/day13.txt")
        .unwrap()
        .trim()
        .parse::<u16>()
        .unwrap();
    println!("Part 1: {}", calc_path(input).unwrap().1);
    println!("Part 2: {}", flood_fill(input, 50).len());
}
