use day07::*;
use std::fs;

fn main() {
    let input = fs::read_to_string("inputs/day07.txt").unwrap();
    let input = parser::data(&input).unwrap();
    println!("Part 1: {}", count_tls(&input));
    println!("Part 2: {}", count_ssl(&input));
}
