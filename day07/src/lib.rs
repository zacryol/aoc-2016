use itertools::Itertools;
use rayon::prelude::*;
use std::collections::HashSet;

#[derive(Debug)]
enum IPv7Segment<'a> {
    Outer(&'a str),
    Hypernet(&'a str),
}

impl<'a> IPv7Segment<'a> {
    fn as_hypernet(&self) -> Option<&[u8]> {
        match self {
            Self::Hypernet(s) => Some(s.as_bytes()),
            _ => None,
        }
    }

    fn as_outer(&self) -> Option<&[u8]> {
        match self {
            Self::Outer(s) => Some(s.as_bytes()),
            _ => None,
        }
    }
}

#[derive(Debug)]
pub struct IPv7Addr<'a>(Vec<IPv7Segment<'a>>);

impl<'a> IPv7Addr<'a> {
    fn is_tls(&self) -> bool {
        self.iter_outer().any(has_abba) && !self.iter_hypernet().any(has_abba)
    }

    fn iter_outer(&self) -> impl ParallelIterator<Item = &[u8]> {
        self.0.par_iter().filter_map(IPv7Segment::as_outer)
    }

    fn iter_hypernet(&self) -> impl ParallelIterator<Item = &[u8]> {
        self.0.par_iter().filter_map(IPv7Segment::as_hypernet)
    }

    fn is_ssl(&self) -> bool {
        let abas = self
            .iter_outer()
            .flat_map_iter(get_abas)
            .collect::<HashSet<_>>();
        let babs = self
            .iter_hypernet()
            .flat_map_iter(get_abas)
            .map(|(a, b)| (b, a))
            .collect::<HashSet<_>>();
        abas.intersection(&babs).any(|_| true)
    }
}

fn has_abba(s: &[u8]) -> bool {
    match s {
        x if x.len() < 4 => false,
        [a, b, c, d, ..] if a == d && b == c && a != b => true,
        [_, tail @ ..] => has_abba(tail),
        [] => false,
    }
}

fn get_abas(s: &[u8]) -> impl Iterator<Item = (u8, u8)> + '_ {
    s.iter()
        .copied()
        .tuple_windows()
        .filter_map(|(a, b, c)| match (a, b, c) {
            (x, y, z) if x == z => Some((x, y)),
            _ => None,
        })
        .unique()
}

pub fn count_tls(addrs: &[IPv7Addr]) -> usize {
    addrs.par_iter().filter(|a| a.is_tls()).count()
}

pub fn count_ssl(addrs: &[IPv7Addr]) -> usize {
    addrs.par_iter().filter(|a| a.is_ssl()).count()
}

pub mod parser {
    use crate::{IPv7Addr, IPv7Segment};
    use nom::{
        branch::alt,
        bytes::complete::tag,
        character::complete::{alpha1, line_ending},
        combinator::{all_consuming, map},
        error::Error as NomErr,
        multi::{many1, separated_list1},
        sequence::delimited,
        Finish, IResult,
    };

    fn hypernet(input: &str) -> IResult<&str, IPv7Segment> {
        map(delimited(tag("["), alpha1, tag("]")), IPv7Segment::Hypernet)(input)
    }

    fn outer(input: &str) -> IResult<&str, IPv7Segment> {
        map(alpha1, IPv7Segment::Outer)(input)
    }

    fn segment(input: &str) -> IResult<&str, IPv7Segment> {
        alt((hypernet, outer))(input)
    }

    fn ipv7_addr(input: &str) -> IResult<&str, IPv7Addr> {
        map(many1(segment), IPv7Addr)(input)
    }

    pub fn data(input: &str) -> Result<Vec<IPv7Addr>, NomErr<&str>> {
        all_consuming(separated_list1(line_ending, ipv7_addr))(input.trim())
            .finish()
            .map(|(_, v)| v)
    }
}
