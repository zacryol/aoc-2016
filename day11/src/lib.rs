use std::{collections::HashSet, hash::Hash};

use itertools::Itertools;
use pathfinding::prelude::astar;

const MIN_FLOOR: u8 = 1;
const MAX_FLOOR: u8 = 4;

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, PartialOrd, Ord)]
enum Type {
    Microchip,
    Generator,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, PartialOrd, Ord)]
struct Item<'a> {
    material: &'a str,
    r#type: Type,
}

#[derive(Debug, Clone, Copy, PartialOrd, Ord)]
pub struct ItemLoc<'a>(u8, Item<'a>);

// Materials are irrelevant for uniqueness of states (only in validity checks), so the manual impls
// here greatly trim state expansion by the astar function.
impl<'a> PartialEq for ItemLoc<'a> {
    fn eq(&self, other: &Self) -> bool {
        self.0 == other.0 && self.1.r#type == other.1.r#type
    }
}

impl<'a> Eq for ItemLoc<'a> {}

impl<'a> Hash for ItemLoc<'a> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.0.hash(state);
        self.1.r#type.hash(state);
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Hash, PartialOrd, Ord)]
pub struct Node<'a> {
    elevator: u8,
    items: Vec<ItemLoc<'a>>,
}

impl<'a> Node<'a> {
    fn dist(&self) -> u8 {
        self.items
            .iter()
            .map(|ItemLoc(fl, _)| MAX_FLOOR.abs_diff(*fl))
            .sum()
    }

    fn is_success(&self) -> bool {
        self.dist() == 0
    }

    fn is_valid(&self) -> bool {
        (MIN_FLOOR..=MAX_FLOOR)
            .map(|fl| {
                self.items
                    .iter()
                    .filter(|it| it.0 == fl)
                    .map(|it| it.1)
                    .collect::<HashSet<_>>()
            })
            .all(|its| {
                let chips = its
                    .iter()
                    .filter(|it| it.r#type == Type::Microchip)
                    .map(|it| it.material)
                    .collect::<HashSet<_>>();
                let generators = its
                    .iter()
                    .filter(|it| it.r#type == Type::Generator)
                    .map(|it| it.material)
                    .collect::<HashSet<_>>();
                chips.difference(&generators).next().is_none() || generators.is_empty()
            })
    }

    fn successors(&self) -> Vec<(Self, u8)> {
        let could_moves: Vec<&ItemLoc> = self
            .items
            .iter()
            .filter(|ItemLoc(fl, _)| *fl == self.elevator)
            .collect_vec();
        could_moves
            .iter()
            .combinations(1)
            .chain(could_moves.iter().combinations(2))
            .flat_map(|mvs| {
                [
                    Node {
                        elevator: self.elevator + 1,
                        items: mvs
                            .iter()
                            .map(|it| ItemLoc(it.0 + 1, it.1))
                            .chain(
                                self.items
                                    .iter()
                                    .filter(|it| !mvs.iter().any(|i2| i2.1 == it.1))
                                    .copied(),
                            )
                            .sorted()
                            .collect_vec(),
                    },
                    Node {
                        elevator: self.elevator - 1,
                        items: mvs
                            .iter()
                            .map(|it| ItemLoc(it.0 - 1, it.1))
                            .chain(
                                self.items
                                    .iter()
                                    .filter(|it| !mvs.iter().any(|i2| i2.1 == it.1))
                                    .copied(),
                            )
                            .sorted()
                            .collect_vec(),
                    },
                ]
            })
            .filter(|n| n.elevator >= MIN_FLOOR && n.elevator <= MAX_FLOOR)
            .filter(Self::is_valid)
            .inspect(|n| assert_eq!(n.items.len(), self.items.len()))
            .map(|n| (n, 1))
            .collect_vec()
    }
}

pub fn solve_path<'a>(init: &'a [ItemLoc]) -> Option<(Vec<Node<'a>>, u8)> {
    let init = Node {
        elevator: 1,
        items: init.to_owned(),
    };
    astar(&init, Node::successors, Node::dist, Node::is_success)
}

pub fn for_part_2<'a>(init: &'a [ItemLoc]) -> Vec<ItemLoc<'a>> {
    static MAT_A: &str = "elerium";
    static MAT_B: &str = "dilithium";
    init.iter()
        .cloned()
        .chain(
            [MAT_A, MAT_B]
                .into_iter()
                .cartesian_product([Type::Generator, Type::Microchip])
                .map(|(mat, tp)| {
                    ItemLoc(
                        1,
                        Item {
                            material: mat,
                            r#type: tp,
                        },
                    )
                }),
        )
        .collect()
}

pub mod parser {
    use crate::{Item, ItemLoc, Type};
    use nom::{
        branch::alt,
        bytes::complete::tag,
        character::complete::{alpha1, line_ending},
        combinator::{all_consuming, map, opt, value},
        multi::separated_list1,
        sequence::{delimited, terminated, tuple},
        Finish, IResult,
    };

    fn material(input: &str) -> IResult<&str, &str> {
        alpha1(input)
    }

    fn generator(input: &str) -> IResult<&str, Item> {
        map(
            delimited(alt((tag("a "), tag("an "))), material, tag(" generator")),
            |m| Item {
                material: m,
                r#type: Type::Generator,
            },
        )(input)
    }

    fn microchip(input: &str) -> IResult<&str, Item> {
        map(
            delimited(
                alt((tag("a "), tag("an "))),
                material,
                tag("-compatible microchip"),
            ),
            |m| Item {
                material: m,
                r#type: Type::Microchip,
            },
        )(input)
    }

    fn item(input: &str) -> IResult<&str, Item> {
        alt((microchip, generator))(input)
    }

    fn item_list(input: &str) -> IResult<&str, Vec<Item>> {
        separated_list1(tuple((opt(tag(",")), tag(" "), opt(tag("and ")))), item)(input)
    }

    fn empty_floor(input: &str) -> IResult<&str, Vec<Item>> {
        value(vec![], tag("nothing relevant"))(input)
    }

    fn floor_contents(input: &str) -> IResult<&str, Vec<Item>> {
        alt((empty_floor, item_list))(input)
    }

    fn floor_number(input: &str) -> IResult<&str, u8> {
        alt((
            value(1, tag("first")),
            value(2, tag("second")),
            value(3, tag("third")),
            value(4, tag("fourth")),
        ))(input)
    }

    fn floor_entry(input: &str) -> IResult<&str, (u8, Vec<Item>)> {
        tuple((
            delimited(tag("The "), floor_number, tag(" floor contains ")),
            terminated(floor_contents, tag(".")),
        ))(input)
    }

    pub fn data(input: &str) -> Result<Vec<ItemLoc>, nom::error::Error<&str>> {
        all_consuming(separated_list1(line_ending, floor_entry))(input.trim())
            .finish()
            .map(|(_, v)| {
                v.into_iter()
                    .flat_map(|(fl, its)| its.into_iter().map(move |it| ItemLoc(fl, it)))
                    .collect()
            })
    }
}
