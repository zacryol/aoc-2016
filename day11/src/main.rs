use day11::*;
use std::fs;

fn main() {
    let input = fs::read_to_string("inputs/day11.txt").unwrap();
    let input = parser::data(&input).unwrap();
    println!("Part 1: {}", solve_path(&input).unwrap().1);
    let input = for_part_2(&input);
    println!("Part 2: {}", solve_path(&input).unwrap().1);
}
