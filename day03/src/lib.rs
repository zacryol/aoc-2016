pub type Tri = [u16; 3];

pub fn parse_input(input: &str) -> Option<Vec<Tri>> {
    fn parse_tri(input: &str) -> Option<Tri> {
        input
            .trim()
            .split(' ')
            .filter(|s| !s.is_empty())
            .filter_map(|s| s.parse::<u16>().ok())
            .collect::<Vec<_>>()
            .try_into()
            .ok()
    }

    input
        .lines()
        .filter(|l| !l.is_empty())
        .map(parse_tri)
        .collect::<Option<Vec<_>>>()
}

pub fn remap_input(input: &[Tri]) -> Vec<Tri> {
    input
        .chunks(3)
        .flat_map(|sq| {
            [
                [sq[0][0], sq[1][0], sq[2][0]],
                [sq[0][1], sq[1][1], sq[2][1]],
                [sq[0][2], sq[1][2], sq[2][2]],
            ]
        })
        .collect()
}

#[allow(clippy::overflow_check_conditional)]
fn is_valid(t: &&Tri) -> bool {
    let (max, sum) = t
        .iter()
        .fold((0, 0), |(max, sum), side| (max.max(*side), sum + side));
    (sum - max) > max
}

pub fn count_valid(tris: &[Tri]) -> usize {
    tris.iter().filter(is_valid).count()
}
