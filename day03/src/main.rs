use day03::*;
use std::fs;

fn main() {
    let input = fs::read_to_string("inputs/day03.txt").unwrap();
    let input = parse_input(&input).unwrap();
    println!("Part 1: {}", count_valid(&input));
    let input = remap_input(&input);
    println!("Part 2: {}", count_valid(&input));
}
