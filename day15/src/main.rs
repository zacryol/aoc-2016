use day15::*;
use std::fs;

fn main() {
    let input = fs::read_to_string("inputs/day15.txt").unwrap();
    let input = parser::data(&input).unwrap();
    println!("Part 1: {}", find_first_t(&input).unwrap());
    let input = for_pt2(input);
    println!("Part 2: {}", find_first_t(&input).unwrap());
}
