pub struct Disc {
    id: u32,
    positions: u32,
    at_t0: u32,
}

impl Disc {
    fn is_open_at(&self, t: u32) -> bool {
        (t + self.id) % self.positions == (self.positions - self.at_t0) % self.positions
    }
}

pub fn find_first_t(discs: &[Disc]) -> Option<u32> {
    (0u32..).find(|t| discs.iter().all(|d| d.is_open_at(*t)))
}

pub fn for_pt2(mut discs: Vec<Disc>) -> Vec<Disc> {
    let new_id = discs.iter().map(|d| d.id).max().unwrap() + 1;
    discs.push(Disc {
        id: new_id,
        positions: 11,
        at_t0: 0,
    });
    discs
}

pub mod parser {
    use crate::Disc;
    use nom::{
        bytes::complete::tag,
        character::complete::{line_ending, u32 as nomu32},
        combinator::{all_consuming, map},
        multi::separated_list1,
        sequence::{delimited, preceded, tuple},
        Finish, IResult,
    };

    fn disc_id(input: &str) -> IResult<&str, u32> {
        preceded(tag("Disc #"), nomu32)(input)
    }

    fn pos_count(input: &str) -> IResult<&str, u32> {
        delimited(tag(" has "), nomu32, tag(" positions;"))(input)
    }

    fn at_t0(input: &str) -> IResult<&str, u32> {
        delimited(tag(" at time=0, it is at position "), nomu32, tag("."))(input)
    }

    fn disc(input: &str) -> IResult<&str, Disc> {
        map(
            tuple((disc_id, pos_count, at_t0)),
            |(id, positions, at_t0)| Disc {
                id,
                positions,
                at_t0,
            },
        )(input)
    }

    pub fn data(input: &str) -> Result<Vec<Disc>, nom::error::Error<&str>> {
        all_consuming(separated_list1(line_ending, disc))(input.trim())
            .finish()
            .map(|(_, v)| v)
    }
}
