//! "Assembunny" assembler and VM
//! used for days 12, 23, and 25 of Advent of Code 2016

use enum_map::{enum_map, Enum, EnumMap};
use std::ops::BitOr;
use std::rc::Rc;

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Code(Rc<[Instr]>);

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
enum Instr {
    Cpy(Read, Reg),
    Inc(Reg),
    Dec(Reg),
    Jnz {
        check: Read,
        jump_by: Read,
    },
    /// Only for day23
    Tgl(Read),
    /// Only for Day 25
    Out(Read),
}

impl Instr {
    fn toggled(self, toggle: bool) -> Option<Self> {
        if !toggle {
            return Some(self);
        }

        assert!(toggle);
        Some(match self {
            Self::Inc(x) => Self::Dec(x),
            Self::Dec(x) => Self::Inc(x),
            Self::Tgl(x) => Self::Inc(x.as_reg()?),
            Self::Jnz { check, jump_by } => Self::Cpy(check, jump_by.as_reg()?),
            Self::Cpy(read, reg) => Self::Jnz {
                check: read,
                jump_by: Read::Reg(reg),
            },
            Self::Out(x) => Self::Inc(x.as_reg()?),
        })
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Enum)]
enum Reg {
    A,
    B,
    C,
    D,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
enum Read {
    Num(i32),
    Reg(Reg),
}

impl Read {
    fn as_reg(self) -> Option<Reg> {
        match self {
            Self::Reg(r) => Some(r),
            Self::Num(_) => None,
        }
    }
}

pub struct Cpu {
    pc: usize,
    code: Code,
    registers: EnumMap<Reg, i32>,
    toggles: Vec<bool>,
}

impl Cpu {
    pub fn new(code: Code) -> Self {
        let toggles = std::iter::repeat(false).take(code.0.len()).collect();
        Self {
            pc: 0,
            code,
            registers: enum_map! {
                Reg::A => 0,
                Reg::B => 0,
                Reg::C => 0,
                Reg::D => 0,
            },
            toggles,
        }
    }

    /// Used by Day 12
    pub fn with_rc(code: Code, rc: i32) -> Self {
        let mut cpu = Self::new(code);
        cpu.registers[Reg::C] = rc;
        cpu
    }

    /// Used by 23 and 25
    pub fn with_ra(code: Code, ra: i32) -> Self {
        let mut cpu = Self::new(code);
        cpu.registers[Reg::A] = ra;
        cpu
    }

    fn read_val(&self, read: Read) -> i32 {
        match read {
            Read::Num(n) => n,
            Read::Reg(r) => self.registers[r],
        }
    }

    fn apply_instr(&mut self, instr: Instr) -> Option<i32> {
        match instr {
            Instr::Cpy(read, reg) => {
                self.registers[reg] = self.read_val(read);
                self.pc += 1;
            }
            Instr::Inc(r) => {
                self.registers[r] += 1;
                self.pc += 1;
            }
            Instr::Dec(r) => {
                self.registers[r] -= 1;
                self.pc += 1;
            }
            Instr::Jnz { check, jump_by } => {
                self.pc = if self.read_val(check) != 0 {
                    self.pc.wrapping_add_signed(self.read_val(jump_by) as isize)
                } else {
                    self.pc + 1
                }
            }
            Instr::Tgl(x) => {
                let tgl_idx = self.pc.wrapping_add_signed(self.read_val(x) as isize);
                self.toggles
                    .get_mut(tgl_idx)
                    .into_iter()
                    .for_each(|tgl| *tgl = !*tgl);
                self.pc += 1;
            }
            Instr::Out(x) => {
                self.pc += 1;
                return Some(self.read_val(x));
            }
        };

        None
    }

    pub fn exec(&mut self) -> &Self {
        loop {
            let Some(instr) = self.code.0.get(self.pc) else {
                return self;
            };

            assert!(self.toggles.len() > self.pc);

            let Some(instr) = instr.toggled(self.toggles[self.pc]) else {
                self.pc += 1;
                continue;
            };

            self.apply_instr(instr);
        }
    }

    pub fn ra(&self) -> i32 {
        self.registers[Reg::A]
    }

    /// Each initial value of `a` causes a 12-step cycle of 1 and 0 (for my input at least)
    /// Each time that intial value is increased by 1, that cycle, considered as a binary number,
    /// with the first value being lsb, increments by 1.
    /// This function returns the bit cycle at a_0 = 0
    fn seed(code: Code) -> u16 {
        Self::with_ra(code, 0)
            .take(12)
            .collect::<Vec<_>>()
            .into_iter()
            .enumerate()
            .map(|(i, b)| (b as u16) << i)
            .reduce(u16::bitor)
            .unwrap()
    }

    pub fn a_for_cycle(code: Code) -> u16 {
        0b101010101010 - Self::seed(code)
    }
}

impl Iterator for Cpu {
    type Item = i32;

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            let instr = self.code.0.get(self.pc)?;

            assert!(self.toggles.len() > self.pc);

            let Some(instr) = instr.toggled(self.toggles[self.pc]) else {
                self.pc += 1;
                continue;
            };

            match self.apply_instr(instr) {
                None => {}
                Some(x) => return Some(x),
            };
        }
    }
}

pub mod parser {
    use crate::{Code, Instr, Read, Reg};
    use nom::{
        branch::alt,
        bytes::complete::tag,
        character::complete::{i32 as nomi32, line_ending, multispace1},
        combinator::{all_consuming, map, value},
        error::ParseError,
        multi::separated_list1,
        sequence::{preceded, separated_pair, tuple},
        Finish, IResult, Parser,
    };

    fn num(input: &str) -> IResult<&str, Read> {
        map(nomi32, Read::Num)(input)
    }

    fn reg(input: &str) -> IResult<&str, Reg> {
        alt((
            value(Reg::A, tag("a")),
            value(Reg::B, tag("b")),
            value(Reg::C, tag("c")),
            value(Reg::D, tag("d")),
        ))(input)
    }

    fn read_reg(input: &str) -> IResult<&str, Read> {
        map(reg, Read::Reg)(input)
    }

    fn read(input: &str) -> IResult<&str, Read> {
        alt((read_reg, num))(input)
    }

    fn instruction(input: &str) -> IResult<&str, Instr> {
        fn inst_inner<'a, T, P, F, E>(
            inst_name: &'a str,
            parser: P,
            f: F,
        ) -> impl FnMut(&'a str) -> IResult<&'a str, Instr, E>
        where
            P: Parser<&'a str, T, E>,
            F: Fn(T) -> Instr,
            E: ParseError<&'a str>,
        {
            map(preceded(tuple((tag(inst_name), multispace1)), parser), f)
        }

        let cpy = inst_inner("cpy", separated_pair(read, multispace1, reg), |(i, o)| {
            Instr::Cpy(i, o)
        });
        let inc = inst_inner("inc", reg, Instr::Inc);
        let dec = inst_inner("dec", reg, Instr::Dec);
        let jnz = inst_inner(
            "jnz",
            separated_pair(read, multispace1, read),
            |(check, jump_by)| Instr::Jnz { jump_by, check },
        );
        let tgl = inst_inner("tgl", read, Instr::Tgl);
        let out = inst_inner("out", read, Instr::Out);

        alt((cpy, inc, dec, jnz, tgl, out))(input)
    }

    pub fn code(input: &str) -> Result<Code, nom::error::Error<&str>> {
        all_consuming(separated_list1(line_ending, instruction))(input.trim())
            .finish()
            .map(|(_, v)| Code(v.into_boxed_slice().into()))
    }
}
