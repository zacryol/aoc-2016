use day08::*;
use std::fs;

fn main() {
    let input = fs::read_to_string("inputs/day08.txt").unwrap();
    let input = parser::data(&input).unwrap();
    let grid = get_result(&input);
    println!("Part 1: {}", count_on(&grid));
    println!("Part 2:");
    print_grid(&grid);
}
