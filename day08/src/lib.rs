use rayon::prelude::*;
use std::ops::ControlFlow;

#[derive(Debug)]
pub enum Command {
    Rect(u8, u8),
    Row { y: u8, amount: u8 },
    Column { x: u8, amount: u8 },
}

mod grid {
    pub const WIDTH: u8 = 50;
    pub const HEIGHT: u8 = 6;
}

type Point = (u8, u8);

fn rev_map_point(p: Point, c: &Command) -> ControlFlow<(), Point> {
    match c {
        Command::Rect(x, y) if *x > p.0 && *y > p.1 => ControlFlow::Break(()),
        Command::Row { y, amount } if *y == p.1 => {
            ControlFlow::Continue(((p.0 + grid::WIDTH - *amount) % grid::WIDTH, p.1))
        }
        Command::Column { x, amount } if *x == p.0 => {
            ControlFlow::Continue((p.0, (p.1 + grid::HEIGHT - *amount) % grid::HEIGHT))
        }
        Command::Rect(_, _) | Command::Column { .. } | Command::Row { .. } => {
            ControlFlow::Continue(p)
        }
    }
}

fn ends_on(p: Point, cs: &[Command]) -> bool {
    cs.iter().rev().try_fold(p, rev_map_point).is_break()
}

type Grid = [[bool; grid::WIDTH as usize]; grid::HEIGHT as usize];

pub fn get_result(cs: &[Command]) -> Grid {
    (0..grid::HEIGHT)
        .into_par_iter()
        .map(|y| {
            (0..grid::WIDTH)
                .into_par_iter()
                .map(|x| ends_on((x, y), cs))
                .collect::<Vec<_>>()
                .try_into()
                .unwrap()
        })
        .collect::<Vec<_>>()
        .try_into()
        .unwrap()
}

pub fn count_on(g: &Grid) -> usize {
    g.iter().flat_map(|r| r.iter()).filter(|x| **x).count()
}

pub fn print_grid(g: &Grid) {
    for row in g.iter() {
        for pix in row.iter() {
            if *pix {
                print!("█");
            } else {
                print!(" ")
            }
        }
        println!();
    }
}

pub mod parser {
    use crate::Command;
    use nom::{
        branch::alt,
        bytes::complete::tag,
        character::complete::{line_ending, u8 as nomu8},
        combinator::{all_consuming, map},
        error::Error as NomErr,
        multi::separated_list1,
        sequence::{preceded, separated_pair as spair},
        Finish, IResult,
    };

    fn rect(input: &str) -> IResult<&str, Command> {
        map(
            preceded(tag("rect "), spair(nomu8, tag("x"), nomu8)),
            |(a, b)| Command::Rect(a, b),
        )(input)
    }

    fn r_row(input: &str) -> IResult<&str, Command> {
        map(
            preceded(tag("row y="), spair(nomu8, tag(" by "), nomu8)),
            |(y, amount)| Command::Row { y, amount },
        )(input)
    }

    fn r_col(input: &str) -> IResult<&str, Command> {
        map(
            preceded(tag("column x="), spair(nomu8, tag(" by "), nomu8)),
            |(x, amount)| Command::Column { x, amount },
        )(input)
    }

    fn rotate(input: &str) -> IResult<&str, Command> {
        preceded(tag("rotate "), alt((r_row, r_col)))(input)
    }

    fn command(input: &str) -> IResult<&str, Command> {
        alt((rotate, rect))(input)
    }

    pub fn data(input: &str) -> Result<Vec<Command>, NomErr<&str>> {
        all_consuming(separated_list1(line_ending, command))(input.trim())
            .finish()
            .map(|(_, v)| v)
    }
}
