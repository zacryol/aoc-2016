use day19::*;
use std::fs;

fn main() {
    let input = fs::read_to_string("inputs/day19.txt").unwrap();
    let input: u64 = input.trim().parse().unwrap();
    println!("Part 1: {}", end_elf(input));
    println!("Part 2: {}", pt2(input as u32));
}
