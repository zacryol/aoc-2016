use std::collections::VecDeque;

#[derive(Debug, PartialEq, Eq)]
struct State {
    current: u64,
    index: u64,
    trace: u64,
    last_removed_one: bool,
    should_remove_one: bool,
}

impl State {
    fn new(input: u64) -> Self {
        Self {
            current: input,
            index: 0,
            trace: 1,
            last_removed_one: false,
            should_remove_one: false,
        }
    }

    fn advance(self) -> Self {
        if self.current == 1 {
            self
        } else if self.should_remove_one {
            Self {
                current: self.current / 2,
                index: self.index + 1,
                trace: (self.trace) | (1 << self.index),
                last_removed_one: true,
                should_remove_one: self.current % 2 == 0,
            }
        } else {
            Self {
                current: self.current - (self.current / 2),
                index: self.index + 1,
                last_removed_one: false,
                should_remove_one: self.current % 2 != 0,
                trace: self.trace,
            }
        }
    }

    fn collapse(self) -> Self {
        if self.current == 1 {
            self
        } else {
            self.advance().collapse()
        }
    }
}

pub fn end_elf(input: u64) -> u64 {
    State::new(input).collapse().trace
}

// Did math for pt1, now it's time to brute force
pub fn pt2(input: u32) -> u32 {
    let mut nums = (1..=input).collect::<VecDeque<_>>();
    let mut to_go = 1;
    while nums.len() > 1 {
        let go_idx = loop {
            match nums.binary_search(&to_go) {
                Ok(i) => break i,
                Err(_) => to_go = (to_go + 1) % (input + 1),
            }
        };
        let rm_idx = (go_idx + (nums.len() / 2)) % nums.len();
        nums.remove(rm_idx);
        to_go += 1;
    }

    nums[0]
}
