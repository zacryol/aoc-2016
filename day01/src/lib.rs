use std::{collections::HashSet, ops::ControlFlow};

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
enum FacingDir {
    North,
    South,
    East,
    West,
}

impl FacingDir {
    fn turned(self, td: TurnDir) -> Self {
        match (self, td) {
            (Self::North, TurnDir::Left) | (Self::South, TurnDir::Right) => Self::West,
            (Self::North, TurnDir::Right) | (Self::South, TurnDir::Left) => Self::East,
            (Self::East, TurnDir::Left) | (Self::West, TurnDir::Right) => Self::North,
            (Self::East, TurnDir::Right) | (Self::West, TurnDir::Left) => Self::South,
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
pub enum TurnDir {
    Left,
    Right,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
struct Pos(FacingDir, i32, i32);

impl Pos {
    fn turned(self, td: TurnDir) -> Self {
        Self(self.0.turned(td), self.1, self.2)
    }

    fn forward(self, dist: i32) -> Self {
        match self {
            Self(FacingDir::North, x, y) => Self(FacingDir::North, x, y - dist),
            Self(FacingDir::South, x, y) => Self(FacingDir::South, x, y + dist),
            Self(FacingDir::East, x, y) => Self(FacingDir::East, x + dist, y),
            Self(FacingDir::West, x, y) => Self(FacingDir::West, x - dist, y),
        }
    }

    fn forward_with_in_betweens(self, dist: i32) -> Box<dyn Iterator<Item = Self>> {
        match self {
            Self(FacingDir::North, x, y) => Box::new(
                ((y - dist)..y)
                    .rev()
                    .map(move |y| Self(FacingDir::North, x, y)),
            ),
            Self(FacingDir::South, x, y) => {
                Box::new(((y + 1)..=(y + dist)).map(move |y| Self(FacingDir::South, x, y)))
            }
            Self(FacingDir::East, x, y) => {
                Box::new(((x + 1)..=(x + dist)).map(move |x| Self(FacingDir::East, x, y)))
            }
            Self(FacingDir::West, x, y) => Box::new(
                ((x - dist)..x)
                    .rev()
                    .map(move |x| Self(FacingDir::West, x, y)),
            ),
        }
    }

    fn into_dist(self) -> i32 {
        self.1.abs() + self.2.abs()
    }

    fn as_location(self) -> (i32, i32) {
        (self.1, self.2)
    }
}

pub fn parse_input(input: &str) -> Option<Vec<(TurnDir, i32)>> {
    input
        .trim()
        .split(", ")
        .map(|s| {
            let (dir, dist) = s.split_at(1);
            let dir = match dir {
                "L" => TurnDir::Left,
                "R" => TurnDir::Right,
                _ => unreachable!(),
            };
            let dist = dist.parse().ok()?;
            Some((dir, dist))
        })
        .collect()
}

pub fn get_total_distance(steps: &[(TurnDir, i32)]) -> i32 {
    steps
        .iter()
        .fold(Pos(FacingDir::North, 0, 0), |p, (dir, dist)| {
            p.turned(*dir).forward(*dist)
        })
        .into_dist()
}

pub fn dist_to_double_vis(steps: &[(TurnDir, i32)]) -> Option<i32> {
    let (x0, y0) = (0, 0);
    match steps.iter().try_fold(
        (HashSet::from([(x0, y0)]), Pos(FacingDir::North, x0, y0)),
        |(mut vis, p), (dir, dist)| {
            let p = p.turned(*dir);
            p.forward_with_in_betweens(*dist).try_for_each(|ip| {
                if vis.insert(ip.as_location()) {
                    ControlFlow::Continue(())
                } else {
                    ControlFlow::Break(ip)
                }
            })?;
            ControlFlow::Continue((vis, p.forward(*dist)))
        },
    ) {
        ControlFlow::Continue(_) => None,
        ControlFlow::Break(p) => Some(p.into_dist()),
    }
}
