use day01::*;
use std::fs;

fn main() {
    let input = fs::read_to_string("inputs/day01.txt").unwrap();
    let input = parse_input(&input).unwrap();
    println!("Part 1: {}", get_total_distance(&input));
    println!("Part 2: {}", dist_to_double_vis(&input).unwrap());
}
