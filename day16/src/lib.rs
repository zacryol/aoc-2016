use rayon::prelude::*;
use std::cmp::Ordering;

pub const PT1_DSK: usize = 272;
pub const PT2_DSK: usize = 35651584;

fn get_expanded_bit_at(seed: &[u8], at: usize, len: usize) -> bool {
    if len <= seed.len() {
        return seed[at] == b'1';
    }

    let mid = (len - 1) / 2;

    match at.cmp(&mid) {
        Ordering::Less => get_expanded_bit_at(seed, at, mid),
        Ordering::Equal => false,
        Ordering::Greater => !get_expanded_bit_at(seed, len - 1 - at, mid),
    }
}

fn get_total_needed(seed_len: usize, base_len: usize) -> usize {
    if seed_len >= base_len {
        seed_len
    } else {
        get_total_needed(seed_len * 2 + 1, base_len)
    }
}

pub fn get_checksum(seed: &[u8], dsklen: usize) -> String {
    let total_len = get_total_needed(seed.len(), dsklen);

    let sumlen = get_checksum_length(dsklen);

    (0..sumlen)
        .into_par_iter()
        .map(|i| {
            if get_checksum_bit_at(seed, i, sumlen, total_len) {
                '1'
            } else {
                '0'
            }
        })
        .collect()
}

fn get_checksum_length(disk_size: usize) -> usize {
    match disk_size % 2 == 0 {
        true => get_checksum_length(disk_size / 2),
        false => disk_size,
    }
}

fn get_checksum_bit_at(seed: &[u8], at: usize, sumlen: usize, total_len: usize) -> bool {
    if sumlen * 2 >= total_len {
        get_expanded_bit_at(seed, at, total_len)
    } else {
        let sumlen = sumlen * 2;
        let at = at * 2;
        let bits = rayon::join(
            || get_checksum_bit_at(seed, at, sumlen, total_len),
            || get_checksum_bit_at(seed, at + 1, sumlen, total_len),
        );
        bits.0 == bits.1
    }
}
