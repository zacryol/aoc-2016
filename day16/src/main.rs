use day16::*;
use std::fs;

fn main() {
    let input = fs::read_to_string("inputs/day16.txt").unwrap();
    let input = input.trim().as_bytes();
    println!("Part 1: {}", get_checksum(input, PT1_DSK));
    println!("Part 2: {}", get_checksum(input, PT2_DSK));
}
