use day04::*;
use std::fs;

fn main() {
    let input = fs::read_to_string("inputs/day04.txt").unwrap();
    let input = parser::data(&input).unwrap();
    println!("Part 1: {}", valid_ids_sum(&input));
    println!("Part 2: {}", find_north_pole(&input).unwrap());
}
