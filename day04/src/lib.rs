use std::cmp::Ordering;

use itertools::Itertools;

#[derive(Debug)]
pub struct RoomData<'a> {
    name: &'a str,
    id: u16,
    checksum: &'a str,
}

impl<'a> RoomData<'a> {
    fn is_valid(&self) -> bool {
        self.name
            .chars()
            .filter(|c| c.is_alphabetic())
            .counts()
            .into_iter()
            .sorted_by(
                |(char1, count1), (char2, count2)| match count1.cmp(count2) {
                    Ordering::Less => Ordering::Greater,
                    Ordering::Greater => Ordering::Less,
                    Ordering::Equal => char1.cmp(char2),
                },
            )
            .take(5)
            .map(|(c, _)| c)
            .zip(self.checksum.chars())
            .all(|(c1, c2)| c1 == c2)
    }

    fn decrypted_name(&self) -> String {
        self.name
            .chars()
            .map(|c| match c {
                '-' => ' ',
                c => cycle_char(c, self.id),
            })
            .collect()
    }
}

pub fn find_north_pole(rooms: &[RoomData]) -> Option<u16> {
    rooms
        .iter()
        .find(|r| r.decrypted_name() == "northpole object storage")
        .map(|r| r.id)
}

fn cycle_char(c: char, amount: u16) -> char {
    let c = c as u8 + (amount % 26) as u8;
    (if c > b'z' { c - 26 } else { c }) as char
}

pub fn valid_ids_sum(rooms: &[RoomData]) -> u32 {
    rooms
        .iter()
        .filter(|r| r.is_valid())
        .map(|r| r.id as u32)
        .sum()
}

pub mod parser {
    use crate::RoomData;
    use nom::{
        bytes::complete::{tag, take_while},
        character::{
            complete::{alpha1, line_ending, u16 as nomu16},
            is_alphabetic,
        },
        combinator::{all_consuming, map},
        error::Error as NomErr,
        multi::separated_list1,
        sequence::{delimited, tuple},
        Finish, IResult,
    };

    fn checksum(input: &str) -> IResult<&str, &str> {
        delimited(tag("["), alpha1, tag("]"))(input)
    }

    fn sector_id(input: &str) -> IResult<&str, u16> {
        nomu16(input)
    }

    fn is_name_chr(chr: char) -> bool {
        is_alphabetic(chr as u8) || chr == '-'
    }

    fn name(input: &str) -> IResult<&str, &str> {
        map(take_while(is_name_chr), |nm: &str| nm.trim_end_matches('-'))(input)
    }

    fn room_data(input: &str) -> IResult<&str, RoomData> {
        map(
            tuple((name, sector_id, checksum)),
            |(name, id, checksum)| RoomData { name, id, checksum },
        )(input)
    }

    pub fn data(input: &str) -> Result<Vec<RoomData>, NomErr<&str>> {
        all_consuming(separated_list1(line_ending, room_data))(input.trim())
            .finish()
            .map(|(_, v)| v)
    }
}
