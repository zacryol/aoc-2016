use day05::*;
use std::fs;

fn main() {
    let input = fs::read_to_string("inputs/day05.txt").unwrap();
    println!("Part 1: {}", find_password(input.trim()));
    println!("Part 2: {}", find_password_2(input.trim()));
}
