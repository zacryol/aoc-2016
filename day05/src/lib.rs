use itertools::Itertools;

struct Md5Iter {
    current: usize,
    door_id: String,
}

impl Md5Iter {
    fn new(door_id: &str) -> Self {
        Self {
            current: 0,
            door_id: door_id.to_owned(),
        }
    }
}

impl Iterator for Md5Iter {
    type Item = (char, char);

    fn next(&mut self) -> Option<Self::Item> {
        let (current, out) = (self.current..)
            .map(|i| (i, format!("{}{}", self.door_id, i)))
            .map(|(i, s)| (i, format!("{:x}", md5::compute(s.as_bytes()))))
            .find_map(|(i, b)| match b.as_bytes() {
                [b'0', b'0', b'0', b'0', b'0', c, c2, ..] => Some((i, (*c as char, *c2 as char))),
                _ => None,
            })?;
        self.current = current + 1;
        Some(out)
    }
}

pub fn find_password(door_id: &str) -> String {
    Md5Iter::new(door_id).take(8).map(|(c, _)| c).collect()
}

pub fn find_password_2(door_id: &str) -> String {
    Md5Iter::new(door_id)
        .filter(|(c, _)| *c < '8' && *c >= '0')
        .unique_by(|(c, _)| *c)
        .take(8)
        .sorted_by_key(|(c, _)| *c)
        .map(|(_, c)| c)
        .collect()
}
