use day12::*;
use std::fs;

fn main() {
    let input = fs::read_to_string("inputs/day12.txt").unwrap();
    let input = parser::code(&input).unwrap();
    let mut cpu = Cpu::new(input.clone());
    println!("Part 1: {}", cpu.exec().ra());
    let mut cpu = Cpu::with_rc(input, 1);
    println!("Part 2: {}", cpu.exec().ra());
}
