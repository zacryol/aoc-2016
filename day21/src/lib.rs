#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Letter(u8);

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Op {
    SwapP(u8, u8),
    SwapC(Letter, Letter),
    RotateR(i8),
    RotateB(Letter),
    Reverse(u8, u8),
    Move(u8, u8),
}

impl Op {
    fn apply(&self, mut data: Vec<u8>) -> Option<Vec<u8>> {
        match *self {
            Self::SwapP(x, y) => {
                data.swap(x as usize, y as usize);
            }
            Self::SwapC(Letter(a), Letter(b)) => {
                let x = data.iter().position(|i| *i == a)?;
                let y = data.iter().position(|i| *i == b)?;
                data.swap(x, y);
            }
            Self::RotateR(i) => {
                if i < 0 {
                    let i = -i as usize;
                    data.rotate_left(i);
                } else {
                    data.rotate_right(i as usize);
                }
            }
            Self::RotateB(Letter(a)) => {
                let r = data.iter().position(|i| *i == a)?;
                let r = (r + 1 + if r >= 4 { 1 } else { 0 }) % data.len();
                data.rotate_right(r);
            }
            Self::Reverse(a, b) => {
                let (a, b) = (a as usize, b as usize);
                data[a..=b].reverse();
            }
            Self::Move(x, y) => {
                let a = data.remove(x as usize);
                data.insert(y as usize, a);
            }
        }
        Some(data)
    }

    fn apply_rev(&self, mut data: Vec<u8>) -> Option<Vec<u8>> {
        match *self {
            Self::SwapP(x, y) => {
                data.swap(x as usize, y as usize);
            }
            Self::SwapC(Letter(a), Letter(b)) => {
                let x = data.iter().position(|i| *i == a)?;
                let y = data.iter().position(|i| *i == b)?;
                data.swap(x, y);
            }
            Self::RotateR(i) => {
                if i < 0 {
                    let i = -i as usize;
                    data.rotate_right(i);
                } else {
                    data.rotate_left(i as usize);
                }
            }
            Self::RotateB(Letter(a)) => {
                let mut d2 = data.clone();
                d2.rotate_left(1);
                while Self::RotateB(Letter(a)).apply(d2.clone())? != data {
                    d2.rotate_left(1)
                }
                data = d2
            }
            Self::Reverse(a, b) => {
                let (a, b) = (a as usize, b as usize);
                data[a..=b].reverse();
            }
            Self::Move(x, y) => {
                let a = data.remove(y as usize);
                data.insert(x as usize, a);
            }
        }

        Some(data)
    }

    pub fn scramble(init: Vec<u8>, ops: &[Self]) -> Option<String> {
        ops.iter()
            .try_fold(init, |v, o| o.apply(v))
            .and_then(|v| String::from_utf8(v).ok())
    }

    pub fn scramble_rev(init: Vec<u8>, ops: &[Self]) -> Option<String> {
        ops.iter()
            .rev()
            .try_fold(init, |v, o| o.apply_rev(v))
            .and_then(|v| String::from_utf8(v).ok())
    }
}

pub mod parser {
    use crate::{Letter, Op};
    use nom::{
        branch::alt,
        bytes::complete::tag,
        character::complete::{anychar, i8 as nomi8, line_ending, multispace1, u8 as nomu8},
        combinator::{all_consuming, map, value},
        multi::separated_list1,
        sequence::{delimited, preceded, tuple},
        Finish, IResult,
    };

    type Res1<'a> = IResult<&'a str, Op>;

    fn letter(input: &str) -> IResult<&str, Letter> {
        map(anychar, |c| Letter(c as u8))(input)
    }

    fn swap_p(input: &str) -> Res1 {
        map(
            tuple((
                delimited(tag("swap position "), nomu8, tag(" with position ")),
                nomu8,
            )),
            |(x, y)| Op::SwapP(x, y),
        )(input)
    }

    fn swap_c(input: &str) -> Res1 {
        map(
            tuple((
                delimited(tag("swap letter "), letter, tag(" with letter ")),
                letter,
            )),
            |(x, y)| Op::SwapC(x, y),
        )(input)
    }

    fn rotate_r(input: &str) -> Res1 {
        map(
            tuple((
                preceded(
                    tag("rotate "),
                    alt((value(-1, tag("left")), value(1, tag("right")))),
                ),
                delimited(multispace1, nomi8, alt((tag(" steps"), tag(" step")))),
            )),
            |(i, x)| Op::RotateR(i * x),
        )(input)
    }

    fn rotate_b(input: &str) -> Res1 {
        map(
            preceded(tag("rotate based on position of letter "), letter),
            Op::RotateB,
        )(input)
    }

    fn reverse(input: &str) -> Res1 {
        map(
            tuple((
                delimited(tag("reverse positions "), nomu8, tag(" through ")),
                nomu8,
            )),
            |(x, y)| Op::Reverse(x, y),
        )(input)
    }

    fn op_move(input: &str) -> Res1 {
        map(
            tuple((
                delimited(tag("move position "), nomu8, tag(" to position ")),
                nomu8,
            )),
            |(x, y)| Op::Move(x, y),
        )(input)
    }

    fn op(input: &str) -> Res1 {
        alt((swap_p, swap_c, rotate_r, rotate_b, reverse, op_move))(input)
    }

    pub fn data(input: &str) -> Result<Vec<Op>, nom::error::Error<&str>> {
        all_consuming(separated_list1(line_ending, op))(input.trim())
            .finish()
            .map(|(_, v)| v)
    }
}
