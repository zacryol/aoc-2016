use day21::*;
use std::fs;

fn main() {
    let input = fs::read_to_string("inputs/day21.txt").unwrap();
    let input = parser::data(&input).unwrap();
    println!(
        "Part 1: {}",
        Op::scramble("abcdefgh".as_bytes().to_owned(), &input).unwrap()
    );
    println!(
        "Part 2: {}",
        Op::scramble_rev("fbgdceah".as_bytes().to_owned(), &input).unwrap()
    );
}
