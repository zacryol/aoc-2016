use itertools::Itertools;
use std::ops::RangeInclusive;

pub fn get_ranges_collapsed(input: &str) -> impl Iterator<Item = RangeInclusive<u32>> + '_ {
    fn get_ranges(input: &str) -> impl Iterator<Item = RangeInclusive<u32>> + '_ {
        input.trim().lines().map(|l| {
            let (l, u) = l.split_once('-').unwrap();
            (l.parse().unwrap())..=(u.parse().unwrap())
        })
    }
    fn try_merge_ranges(
        a: RangeInclusive<u32>,
        b: RangeInclusive<u32>,
    ) -> Result<RangeInclusive<u32>, (RangeInclusive<u32>, RangeInclusive<u32>)> {
        if *a.end() >= *b.start() - 1 {
            Ok((*a.start())..=(*b.end().max(a.end())))
        } else {
            Err((a, b))
        }
    }
    get_ranges(input)
        .sorted_by_key(|r| *r.start())
        .coalesce(try_merge_ranges)
}

pub fn get_first_allowed(ranges: impl Iterator<Item = RangeInclusive<u32>>) -> u32 {
    ranges.take(1).next().map_or(0, |r| r.end() + 1)
}

pub fn how_many_allowed(ranges: impl Iterator<Item = RangeInclusive<u32>>) -> u32 {
    ([0..=0].into_iter())
        .chain(ranges)
        .chain([u32::MAX..=u32::MAX].into_iter())
        .tuple_windows()
        .map(|(a, b)| b.start().saturating_sub(a.end().saturating_add(1)))
        .sum()
}
