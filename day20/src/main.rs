use day20::*;
use std::fs;

fn main() {
    let input = fs::read_to_string("inputs/day20.txt").unwrap();
    let input = get_ranges_collapsed(&input).collect::<Vec<_>>();
    println!("Part 1: {}", get_first_allowed(input.iter().cloned()));
    println!("Part 2: {}", how_many_allowed(input.iter().cloned()));
}
