use day23::*;
use std::fs;

fn main() {
    let input = fs::read_to_string("inputs/day23.txt").unwrap();
    let input = parser::code(&input).unwrap();
    println!("Part 1: {}", Cpu::with_ra(input.clone(), 7).exec().ra());
    println!("Part 2: {}", Cpu::with_ra(input, 12).exec().ra());
}
