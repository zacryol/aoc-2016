use day17::*;
use std::fs;

fn main() {
    let input = fs::read_to_string("inputs/day17.txt").unwrap();
    let input = input.trim();
    println!("Part 1: {}", find_path(input).unwrap());
    println!("Part 2: {}", longest_path_len(input).unwrap());
}
