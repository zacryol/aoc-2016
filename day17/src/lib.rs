use pathfinding::prelude::astar;

const SIZE: u8 = 3;

#[derive(Debug, PartialEq, Eq, Clone, Hash)]
pub struct Point(u8, u8, String);

impl Point {
    fn successors(&self, code: &str) -> impl Iterator<Item = Self> {
        let is_open = |b| [b'b', b'c', b'd', b'e', b'f'].iter().any(|x| *x == b);
        let Point(x, y, s) = self;
        let x = *x;
        let y = *y;
        let state = format!("{:x}", md5::compute(format!("{code}{s}")));
        let [up, down, left, right, ..] = state.as_bytes() else {
            unreachable!("md5 shouldn't be that short");
        };
        [
            Some(Point(x.wrapping_sub(1), y, s.clone() + "L"))
                .filter(|_| is_open(*left))
                .filter(|p| p.0 <= SIZE),
            Some(Point(x + 1, y, s.clone() + "R"))
                .filter(|_| is_open(*right))
                .filter(|p| p.0 <= SIZE),
            Some(Point(x, y.wrapping_sub(1), s.clone() + "U"))
                .filter(|_| is_open(*up))
                .filter(|p| p.1 <= SIZE),
            Some(Point(x, y + 1, s.clone() + "D"))
                .filter(|_| is_open(*down))
                .filter(|p| p.1 <= SIZE),
        ]
        .into_iter()
        .flatten()
    }
}

pub fn find_path(code: &str) -> Option<String> {
    astar(
        &Point(0, 0, "".to_owned()),
        |p| p.successors(code).map(|p| (p, 1)),
        |Point(x, y, _)| x.abs_diff(SIZE) + y.abs_diff(SIZE),
        |p| matches!(p, Point(SIZE, SIZE, _)),
    )
    .and_then(|(v, _)| Some(v.into_iter().last()?.2))
}

pub fn longest_path_len(code: &str) -> Option<usize> {
    let mut stack = Point(0, 0, "".to_owned())
        .successors(code)
        .collect::<Vec<_>>();

    let mut len = 0;

    while let Some(p) = stack.pop() {
        if matches!(p, Point(SIZE, SIZE, _)) {
            len = len.max(p.2.len());
            continue;
        }
        stack.extend(p.successors(code))
    }

    Some(len).filter(|l| *l > 0)
}
