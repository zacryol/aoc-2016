use std::{
    iter::{repeat_with, Cycle, Take},
    slice::Iter,
    vec::IntoIter,
};

pub struct Decompressor<'a> {
    compressed: Iter<'a, u8>,
    decom: Option<Take<Cycle<IntoIter<u8>>>>,
}

impl<'a> From<&'a [u8]> for Decompressor<'a> {
    fn from(value: &'a [u8]) -> Self {
        Self {
            compressed: value.iter(),
            decom: Default::default(),
        }
    }
}

impl<'a> Iterator for Decompressor<'a> {
    type Item = u8;

    fn next(&mut self) -> Option<Self::Item> {
        self.decom.as_mut().and_then(|d| d.next()).or_else(|| {
            let next_c = *self.compressed.next()?;
            if next_c != b'(' {
                return Some(next_c);
            }
            let decom_dat = repeat_with(|| self.compressed.next())
                .flatten()
                .take_while(|&&b| b != b')')
                .map(|&b| b as char)
                .collect::<String>();
            let (chars, rep_count): (usize, usize) = decom_dat
                .split_once('x')
                .map(|(c, r)| Some((c.parse().ok()?, r.parse().ok()?)))??;
            self.decom = Some(
                repeat_with(|| self.compressed.next())
                    .flatten()
                    .take(chars)
                    .copied()
                    .collect::<Vec<_>>()
                    .into_iter()
                    .cycle()
                    .take(chars * rep_count),
            );
            self.decom.as_mut()?.next()
        })
    }
}
