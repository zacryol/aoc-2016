use day09::*;
use std::fs;

fn main() {
    let input = fs::read_to_string("inputs/day09.txt").unwrap();
    let input = input.trim();
    let decom = part1::Decompressor::from(input.as_bytes());
    println!("Part 1: {}", decom.count());
    let tree = pt2parser::data(input).unwrap();
    println!(
        "Part 2: {}",
        tree.iter().map(|t| t.get_count()).sum::<u64>()
    );
}
