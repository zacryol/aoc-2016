#[derive(Debug)]
pub struct TreeNode {
    rep_data: (u16, u16),
    inner: Vec<TreeNode>,
    other: u16,
}

impl TreeNode {
    pub fn get_count(&self) -> u64 {
        let (used_inner, inner_len) =
            self.inner
                .iter()
                .fold((0u64, 0u64), |(used, total_len), tn| {
                    let tc = tn.get_count();
                    (
                        used + tn.rep_data.0 as u64
                            + format!("({}x{})", tn.rep_data.0, tn.rep_data.1).len() as u64,
                        total_len + tc,
                    )
                });
        let total_inner_len = self.rep_data.0 as u64 - used_inner + inner_len;
        total_inner_len * self.rep_data.1 as u64 + self.other as u64
    }
}

pub mod pt2parser {
    use nom::{
        bytes::complete::{tag, take, take_until},
        character::complete::u16 as nomu16,
        combinator::map,
        error::Error as NomErr,
        multi::many0,
        sequence::{delimited, separated_pair},
        Finish, IResult,
    };

    use crate::TreeNode;

    fn decom_data(input: &str) -> IResult<&str, (u16, u16)> {
        delimited(tag("("), separated_pair(nomu16, tag("x"), nomu16), tag(")"))(input)
    }

    fn tree_node(input: &str) -> IResult<&str, TreeNode> {
        let (input, ignored) = take_until("(")(input)?;
        let (input, data) = decom_data(input)?;
        let (input, to_decom) = take(data.0)(input)?;
        let (_, tn) = map(many0(tree_node), move |v| TreeNode {
            rep_data: data,
            inner: v,
            other: ignored.len() as u16,
        })(to_decom)?;
        Ok((input, tn))
    }

    pub fn data(input: &str) -> Result<Vec<TreeNode>, NomErr<&str>> {
        many0(tree_node)(input).finish().map(|(_, v)| v)
    }
}

pub mod part1;
