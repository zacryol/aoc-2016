use itertools::Itertools;
use rayon::prelude::*;
use std::collections::VecDeque;

const CHUNK_SIZE: usize = 1000;

fn next_thousand(
    base: &str,
    from: usize,
    extra_hashings: usize,
) -> impl Iterator<Item = (usize, String)> {
    (from..(from + CHUNK_SIZE))
        .into_par_iter()
        .map(move |i| {
            (
                i,
                (0..=extra_hashings).fold(format!("{base}{i}"), |ds, _| {
                    format!("{:x}", md5::compute(ds))
                }),
            )
        })
        .collect::<Vec<_>>()
        .into_iter()
        .sorted_by_key(|(i, _d)| *i)
}

fn find_3_in_row(data: impl AsRef<[u8]>) -> Option<u8> {
    data.as_ref()
        .iter()
        .copied()
        .tuple_windows()
        .filter_map(|(a, b, c)| if a == b && b == c { Some(a) } else { None })
        .next()
}

fn has_5_in_row(data: impl AsRef<[u8]>, which: u8) -> bool {
    data.as_ref()
        .iter()
        .copied()
        .tuple_windows()
        .any(|(a, b, c, d, e)| [a, b, c, d, e] == [b, c, d, e, a] && a == which)
}

pub fn find_nth_key(base: &str, n: usize, extra_hashings: usize) -> usize {
    let mut q = [
        next_thousand(base, 0, extra_hashings),
        next_thousand(base, CHUNK_SIZE, extra_hashings),
    ]
    .into_iter()
    .flatten()
    .collect::<VecDeque<_>>();

    let mut found = vec![];

    loop {
        found.extend(
            (0..CHUNK_SIZE)
                .into_par_iter()
                .filter_map(|i| {
                    Some((
                        q[i].0,
                        find_3_in_row(q[i].1.as_bytes())?,
                        q.par_iter().enumerate().filter_map(move |(ii, qi)| {
                            if ii > i && ii <= i + CHUNK_SIZE {
                                Some(&qi.1)
                            } else {
                                None
                            }
                        }),
                    ))
                })
                .filter_map(|(idx, char, iter)| {
                    if iter.any(|ds| has_5_in_row(ds.as_bytes(), char)) {
                        Some(idx)
                    } else {
                        None
                    }
                })
                .collect::<Vec<_>>(),
        );
        if let Some(x) = found.get(n - 1) {
            return *x;
        }

        q.drain(0..CHUNK_SIZE);

        q.extend(next_thousand(base, q.back().unwrap().0 + 1, extra_hashings));
        //assert_eq!(q.len(), 2000);
    }
}
