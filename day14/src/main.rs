use day14::*;
use std::fs;

fn main() {
    let input = fs::read_to_string("inputs/day14.txt").unwrap();
    println!("Part 1: {}", find_nth_key(input.trim(), 64, 0));
    println!("Part 2: {}", find_nth_key(input.trim(), 64, 2016));
}
