use day18::*;
use std::fs;

fn main() {
    let input = fs::read_to_string("inputs/day18.txt").unwrap();
    let input = parse_input(&input);
    println!("Part 1: {}", count_safe(&input, 40));
    println!("Part 2: {}", count_safe(&input, 400_000));
}
