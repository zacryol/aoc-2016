use std::iter;

use itertools::Itertools;

// I put an actual safe tile on each end to make the checks easier
fn with_edge(it: impl Iterator<Item = bool>) -> impl Iterator<Item = bool> {
    fn edge() -> impl Iterator<Item = bool> {
        iter::once(false)
    }
    edge().chain(it).chain(edge())
}

pub fn parse_input(input: &str) -> Vec<bool> {
    fn is_trap(c: char) -> bool {
        match c {
            '^' => true,
            '.' => false,
            _ => unreachable!(),
        }
    }
    with_edge(input.trim().chars().map(is_trap)).collect()
}

pub fn count_safe(init: &[bool], rows: usize) -> usize {
    fn next_row(prev: &[bool]) -> Vec<bool> {
        with_edge(prev.iter().tuple_windows().map(|row| {
            matches!(
                row,
                (true, true, false)
                    | (false, true, true)
                    | (true, false, false)
                    | (false, false, true)
            )
        }))
        .collect()
    }
    fn safe_on_row(row: &[bool]) -> usize {
        // Minus 2 is to remove the fake safes that with_edge() adds on
        row.iter().filter(|t| !**t).count() - 2
    }
    iter::successors(Some(init.to_owned()), |r| Some(next_row(r)))
        .take(rows)
        .map(|r| safe_on_row(&r))
        .sum()
}
