use day24::*;
use std::fs;

fn main() {
    let input = fs::read_to_string("inputs/day24.txt").unwrap();
    let input = Grid::make_from_input(&input);
    println!("Part 1: {}", input.shortest_full_path(false));
    println!("Part 2: {}", input.shortest_full_path(true));
}
