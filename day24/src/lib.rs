use itertools::Itertools;
use pathfinding::prelude::astar;
use std::collections::HashMap;

#[derive(Debug)]
enum GridSquare {
    Wall,
    Open,
    Node(u8),
}

impl TryFrom<char> for GridSquare {
    type Error = ();

    fn try_from(value: char) -> Result<Self, Self::Error> {
        match value {
            '.' => Ok(Self::Open),
            '#' => Ok(Self::Wall),
            n @ '0'..='9' => Ok(Self::Node(n as u8 - b'0')),
            _ => Err(()),
        }
    }
}

impl GridSquare {
    fn is_traversable(&self) -> bool {
        !matches!(self, GridSquare::Wall)
    }
}

#[derive(Debug)]
pub struct Grid(Vec<Vec<GridSquare>>, HashMap<u8, (usize, usize)>);

impl Grid {
    pub fn make_from_input(input: &str) -> Self {
        let mut nodes = HashMap::new();
        Self(
            input
                .lines()
                .enumerate()
                .map(|(y, l)| {
                    l.chars()
                        .map(|c| c.try_into().unwrap())
                        .enumerate()
                        .inspect(|(x, gs)| {
                            if let GridSquare::Node(n) = gs {
                                nodes.insert(*n, (*x, y));
                            }
                        })
                        .map(|t| t.1)
                        .collect_vec()
                })
                .collect(),
            nodes,
        )
    }

    pub fn shortest_full_path(&self, r#return: bool) -> usize {
        let paths = self.build_paths();
        let nodes = self.1.keys().copied().collect_vec();

        nodes
            .iter()
            .filter(|n| **n != 0)
            .permutations(nodes.len() - 1)
            .map(|path| {
                std::iter::once(0u8)
                    .chain(path.into_iter().copied())
                    .chain(if r#return { vec![0u8] } else { vec![] }.into_iter())
                    .tuple_windows::<(_, _)>()
                    .map(|t| paths[&t] as u16)
                    .sum::<u16>()
            })
            .min()
            .unwrap()
            .into()
    }

    fn build_paths(&self) -> HashMap<(u8, u8), usize> {
        self.1
            .iter()
            .cartesian_product(self.1.iter())
            .map(|(from, to)| {
                (
                    (*from.0, *to.0),
                    self.shortest_path(from.1, to.1).unwrap().1,
                )
            })
            .collect()
    }

    fn shortest_path(
        &self,
        from: &(usize, usize),
        to: &(usize, usize),
    ) -> Option<(Vec<(usize, usize)>, usize)> {
        astar(
            from,
            |&(x, y)| {
                [(x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1)]
                    .into_iter()
                    .filter(|(x, y)| self.0[*y][*x].is_traversable())
                    .map(|p| (p, 1))
            },
            |p| p.0.abs_diff(to.0) + p.1.abs_diff(to.1),
            |&p| p == *to,
        )
    }
}
