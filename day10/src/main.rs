use day10::*;
use std::fs;

// Display impls are for generating mermaid.live flowchart syntax, for visualizations.

fn main() {
    let input = fs::read_to_string("inputs/day10.txt").unwrap();
    let input = parser::rules(&input).unwrap();
    let full_graph = build_graph(&input);
    println!(
        "Part 1: {}",
        full_graph
            .iter()
            .find(|kb| kb.compares((Value(17), Value(61))))
            .map(KnownBot::id)
            .unwrap()
    );
    println!(
        "Part 2: {}",
        [0, 1, 2]
            .into_iter()
            .map(|i| find_output_chip(&full_graph, Out(i)).0 as u16)
            .product::<u16>()
    );
    //full_graph.iter().for_each(|b| println!("{b}"));
}
