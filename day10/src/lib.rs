use itertools::Itertools;
use rules::{Bot, BotDst, Rule, Rules};
pub use rules::{Out, Value};
use std::{
    collections::{HashSet, VecDeque},
    fmt::{self, Display},
};

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
pub struct KnownBot {
    id: Bot,
    receives: (Value, Value),
    sends: (BotDst, BotDst),
}

impl KnownBot {
    pub fn compares(&self, pair: (Value, Value)) -> bool {
        self.receives == pair
    }

    pub fn id(&self) -> u8 {
        self.id.0
    }
}

impl Display for KnownBot {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "    B{} --> |{}| {}\n    B{} --> |{}| {}",
            self.id.0,
            self.receives.0 .0,
            self.sends.0,
            self.id.0,
            self.receives.1 .0,
            self.sends.1
        )
    }
}

pub fn find_output_chip(graph: &HashSet<KnownBot>, o: Out) -> Value {
    graph
        .iter()
        .flat_map(|kb| [(kb.receives.0, kb.sends.0), (kb.receives.1, kb.sends.1)])
        .find_map(|(r, s)| if s == BotDst::Out(o) { Some(r) } else { None })
        .unwrap()
}

pub fn build_graph(rules: &Rules) -> HashSet<KnownBot> {
    let (vals, rules) = (rules::value_rules(rules), rules::bot_rules(rules));
    let mut knowns = {
        let b0 = vals
            .iter()
            .map(|vr| match vr {
                Rule::Val(_, b) => *b,
                _ => unreachable!(),
            })
            .duplicates()
            .exactly_one()
            .unwrap();
        let receives: (Value, Value) = vals
            .iter()
            .filter_map(|vr| match vr {
                Rule::Val(v, b) if b0 == *b => Some(*v),
                Rule::Bot(_, _, _) => unreachable!(),
                _ => None,
            })
            .next_tuple()
            .unwrap();
        let sends = match rules[&b0] {
            Rule::Bot(_, l, h) => (*l, *h),
            _ => unreachable!(),
        };
        HashSet::from([KnownBot {
            id: b0,
            receives,
            sends,
        }])
    };

    let mut q = vals
        .into_iter()
        .filter_map(|vr| match vr {
            Rule::Bot(_, _, _) => unreachable!(),
            Rule::Val(v, b) if *b != knowns.iter().next().unwrap().id => Some((*v, *b)),
            _ => None,
        })
        .collect::<VecDeque<(Value, Bot)>>();
    while let Some((value0, bot)) = q.pop_front() {
        if knowns.iter().any(|kb| kb.id == bot) {
            continue;
        }
        let new_knowns = knowns
            .iter()
            .filter_map(|kb| match kb {
                KnownBot {
                    id: _,
                    receives: (_, r),
                    sends: (_, s),
                }
                | KnownBot {
                    id: _,
                    receives: (r, _),
                    sends: (s, _),
                } if *s == BotDst::Bot(bot) && *r != value0 => Some(KnownBot {
                    id: bot,
                    receives: (value0.min(*r), value0.max(*r)),
                    sends: match rules[&bot] {
                        Rule::Bot(_, l, h) => (*l, *h),
                        _ => unreachable!(),
                    },
                }),
                _ => None,
            })
            .unique()
            .collect::<Vec<_>>();
        if new_knowns.is_empty() {
            q.push_back((value0, bot));
            continue;
        }

        for kb in new_knowns.into_iter() {
            let (s0, s1) = kb.sends;
            let (r0, r1) = kb.receives;
            [(s0, r0), (s1, r1)].into_iter().for_each(|(s, r)| match s {
                BotDst::Out(_) => (),
                BotDst::Bot(b) => q.push_back((r, b)),
            });
            knowns.insert(kb);
        }
    }
    knowns
}

mod rules {
    use std::{
        collections::HashMap,
        fmt::{self, Display},
    };

    #[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub struct Value(pub u8);
    #[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub struct Bot(pub u8);
    #[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub struct Out(pub u8);

    #[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
    pub enum BotDst {
        Bot(Bot),
        Out(Out),
    }

    impl Display for BotDst {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            match self {
                Self::Bot(Bot(b)) => write!(f, "B{b}"),
                Self::Out(Out(o)) => write!(f, "O{o}"),
            }
        }
    }

    #[derive(Debug, Clone, Copy, PartialEq, Eq)]
    pub enum Rule {
        Val(Value, Bot),
        Bot(Bot, BotDst, BotDst),
    }

    #[derive(Debug)]
    pub struct Rules(pub Vec<Rule>);

    impl Display for Rule {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            match self {
                Self::Val(Value(v), Bot(b)) => write!(f, "    V{v} --> B{b}"),
                Self::Bot(Bot(b), l, h) => {
                    write!(f, "    B{b} --> |low| {l}\n    B{b} --> |high| {h}")
                }
            }
        }
    }

    pub(super) fn value_rules(rules: &Rules) -> Vec<&Rule> {
        rules
            .0
            .iter()
            .filter(|r| matches!(r, Rule::Val(_, _)))
            .collect()
    }

    pub(super) fn bot_rules(rules: &Rules) -> HashMap<Bot, &Rule> {
        rules
            .0
            .iter()
            .filter_map(|r| match r {
                Rule::Bot(b, _, _) => Some((*b, r)),
                _ => None,
            })
            .collect()
    }
}

pub mod parser {
    use crate::rules::*;
    use nom::{
        branch::alt,
        bytes::complete::tag,
        character::complete::{line_ending, u8 as nomu8},
        combinator::{all_consuming, map},
        multi::separated_list1,
        sequence::{preceded, separated_pair, tuple},
        IResult,
    };

    fn bot(input: &str) -> IResult<&str, Bot> {
        preceded(tag("bot "), map(nomu8, Bot))(input)
    }

    fn val(input: &str) -> IResult<&str, Value> {
        preceded(tag("value "), map(nomu8, Value))(input)
    }

    fn out(input: &str) -> IResult<&str, Out> {
        preceded(tag("output "), map(nomu8, Out))(input)
    }

    fn bot_dst(input: &str) -> IResult<&str, BotDst> {
        alt((map(bot, BotDst::Bot), map(out, BotDst::Out)))(input)
    }

    fn val_rule(input: &str) -> IResult<&str, Rule> {
        map(separated_pair(val, tag(" goes to "), bot), |(v, b)| {
            Rule::Val(v, b)
        })(input)
    }

    fn bot_rule(input: &str) -> IResult<&str, Rule> {
        map(
            tuple((
                bot,
                preceded(tag(" gives low to "), bot_dst),
                preceded(tag(" and high to "), bot_dst),
            )),
            |(b, l, h)| Rule::Bot(b, l, h),
        )(input)
    }

    fn rule(input: &str) -> IResult<&str, Rule> {
        alt((bot_rule, val_rule))(input)
    }

    pub fn rules(input: &str) -> Result<Rules, nom::Err<nom::error::Error<&str>>> {
        all_consuming(separated_list1(line_ending, rule))(input.trim()).map(|(_, v)| Rules(v))
    }
}
