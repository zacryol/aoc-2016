use day02::*;
use std::fs;

fn main() {
    let input = fs::read_to_string("inputs/day02.txt").unwrap();
    let input: Vec<_> = input.lines().filter(|l| !l.is_empty()).collect();

    println!("Part 1: {}", make_code_1(&input));
    println!("Part 2: {}", make_code_2(&input));
}
