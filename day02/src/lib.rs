use std::ops::Add;

pub enum Dir {
    Up,
    Down,
    Left,
    Right,
}

impl Dir {
    fn from_char(c: char) -> Option<Self> {
        match c {
            'U' => Some(Self::Up),
            'D' => Some(Self::Down),
            'L' => Some(Self::Left),
            'R' => Some(Self::Right),
            _ => None,
        }
    }

    fn line_iter(input: &str) -> impl Iterator<Item = Self> + '_ {
        input.chars().filter_map(Self::from_char)
    }
}

type Point = (u8, u8);

fn move_point_1(p: Point, d: Dir) -> Point {
    match d {
        Dir::Left => (p.0.saturating_sub(1), p.1),
        Dir::Right => (p.0.add(1).min(2), p.1),
        Dir::Up => (p.0, p.1.saturating_sub(1)),
        Dir::Down => (p.0, p.1.add(1).min(2)),
    }
}

fn move_point_2(p: Point, d: Dir) -> Point {
    match (p, d) {
        ((x @ 0..=1, y), Dir::Right)
        | ((x @ 2, y @ 1..=3), Dir::Right)
        | ((x @ 3, y @ 2), Dir::Right) => (x + 1, y),
        ((x @ 3..=4, y), Dir::Left)
        | ((x @ 2, y @ 1..=3), Dir::Left)
        | ((x @ 1, y @ 2), Dir::Left) => (x - 1, y),
        ((x, y @ 0..=1), Dir::Down)
        | ((x @ 1..=3, y @ 2), Dir::Down)
        | ((x @ 2, y @ 3), Dir::Down) => (x, y + 1),
        ((x, y @ 3..=4), Dir::Up) | ((x @ 1..=3, y @ 2), Dir::Up) | ((x @ 2, y @ 1), Dir::Up) => {
            (x, y - 1)
        }
        _ => p,
    }
}

fn as_key_1(p: Point) -> char {
    match p {
        (0, 0) => '1',
        (1, 0) => '2',
        (2, 0) => '3',
        (0, 1) => '4',
        (1, 1) => '5',
        (2, 1) => '6',
        (0, 2) => '7',
        (1, 2) => '8',
        (2, 2) => '9',
        _ => unreachable!(),
    }
}

fn as_key_2(p: Point) -> char {
    match p {
        (2, 0) => '1',
        (1, 1) => '2',
        (2, 1) => '3',
        (3, 1) => '4',
        (0, 2) => '5',
        (1, 2) => '6',
        (2, 2) => '7',
        (3, 2) => '8',
        (4, 2) => '9',
        (1, 3) => 'A',
        (2, 3) => 'B',
        (3, 3) => 'C',
        (2, 4) => 'D',
        _ => unreachable!(),
    }
}

fn one_path<F: Fn(Point, Dir) -> Point>(input: &str, start: Point, f: F) -> Point {
    Dir::line_iter(input).fold(start, f)
}

fn make_code<'a, F, F2>(
    paths: &'a [&str],
    init: Point,
    f1: F,
    f2: F2,
) -> impl Iterator<Item = char> + 'a
where
    F: Fn(Point, Dir) -> Point + Copy + 'a,
    F2: Fn(Point) -> char + 'a,
{
    std::iter::successors(Some(init), {
        let mut it = paths.iter();
        move |p| Some(one_path(it.next()?, *p, f1))
    })
    .map(f2)
    .skip(1)
}

pub fn make_code_1(paths: &[&str]) -> String {
    make_code(paths, (1, 1), move_point_1, as_key_1).collect()
}

pub fn make_code_2(paths: &[&str]) -> String {
    make_code(paths, (0, 2), move_point_2, as_key_2).collect()
}
