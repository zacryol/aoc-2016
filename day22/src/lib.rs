use itertools::Itertools;
use pathfinding::prelude::astar;
use rayon::prelude::*;

// Tried using astar for the whole of Part 2 at first, code from that is commented out.

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
pub struct Node {
    x: u8,
    y: u8,
    size: u16,
    used: u16,
    avail: u16,
    use_percent: u8,
}

impl Node {
    fn is_viable_with(&self, b: &Self) -> bool {
        let a = self;
        a.used != 0 && a != b && a.used <= b.avail
    }

    fn id(&self) -> (u8, u8) {
        (self.x, self.y)
    }

    /*
    fn with_data_from(&self, other: &Self) -> Option<Self> {
        let used = self.used + other.used;
        let avail = self.avail.checked_sub(other.used)?;
        Some(Self {
            used,
            avail,
            use_percent: (avail * 100 / self.size) as u8,
            ..*self
        })
    }

    fn with_data_removed(&self) -> Self {
        Self {
            used: 0,
            avail: self.size,
            use_percent: 0,
            ..*self
        }
    }
    */
}

pub fn count_viable_pairs(nodes: &[Node]) -> usize {
    nodes
        .iter()
        .permutations(2)
        .map(|v| match v[..] {
            [a, b] => (a, b),
            _ => unreachable!(),
        })
        .collect_vec()
        .into_par_iter()
        .filter(|(a, b)| a.is_viable_with(b))
        .count()
}

type Grid = Vec<Vec<Node>>;
//type State = (Grid, (usize, usize));

fn grid_collect(nodes: &[Node]) -> Grid {
    nodes
        .iter()
        .map(|n| (n.y, *n))
        .into_group_map()
        .into_iter()
        .sorted_by_key(|(y, _)| *y)
        .map(|(_, mut v)| {
            v.par_sort_unstable_by_key(|n| n.x);
            v
        })
        .collect_vec()
}

/*
fn successors(grid: &Grid, goal: (usize, usize)) -> Vec<State> {
    (0..grid[0].len())
        .cartesian_product(0..grid.len())
        .permutations(2)
        .map(|v| match v[..] {
            [from, to] => (from, to),
            _ => unreachable!(),
        })
        .filter(|(from, to)| {
            (from.0.abs_diff(to.0) <= 1 && from.1.abs_diff(to.1) <= 1)
                && (from.0 == to.0 || from.1 == to.1)
                && (*from != *to)
        })
        .collect_vec()
        .into_par_iter()
        .filter_map(|(from, to)| {
            let from_n = grid[from.1][from.0];
            if from_n.used == 0 {
                return None;
            }

            if from.1 > to.1 && to.1 > 3 {
                return None;
            }

            let new_to = grid[to.1][to.0].with_data_from(&from_n)?;
            let new_from = from_n.with_data_removed();

            let mut g = grid.clone();
            g[from.1][from.0] = new_from;
            g[to.1][to.0] = new_to;

            Some((g, if from == goal { to } else { goal }))
        })
        .collect()
}
*/

fn find_empty_node(g: &Grid) -> (usize, usize) {
    g.iter()
        .flat_map(|v| v.iter())
        .find(|n| n.use_percent == 0)
        .map(|n| (n.id().0 as usize, n.id().1 as usize))
        .unwrap()
}

/*
fn get_shortest_data_path(nodes: Grid) -> Option<(Vec<State>, usize)> {
    let goal_init = {
        let goal_init = nodes[0].last().unwrap().id();
        (goal_init.0 as usize, goal_init.1 as usize)
    };
    astar(
        &(nodes, goal_init),
        |(grid, goal)| successors(grid, *goal).into_iter().map(|gg| (gg, 1)),
        |(g, goal)| {
            let em = dbg!(find_empty_node(g));
            goal.0
                + goal.1
                + if goal.0 > 2 {
                    (em.1 as usize).abs_diff(goal.1) / 3
                } else {
                    0
                }
        },
        |(_, goal)| *goal == (0, 0),
    )
}
*/

pub fn get_move_steps(nodes: &[Node]) -> Option<usize> {
    let nodes = grid_collect(nodes);
    let emp = find_empty_node(&nodes);
    let goal_init = {
        let goal_init = nodes[0].last().unwrap().id();
        (goal_init.0 as usize, goal_init.1 as usize)
    };
    let emp_final = (goal_init.0 - 1, goal_init.1);
    // It's a sliding puzzle
    // First, get empty slot to left of goal node.
    let path_to_near_top_right = astar(
        &emp,
        |p| {
            (-1..=1isize)
                .cartesian_product(-1..=1isize)
                .filter(|(x, y)| x.abs() * y.abs() == 0 && x.abs() + y.abs() == 1)
                .filter_map(|(x, y)| Some((p.0.checked_add_signed(x)?, p.1.checked_add_signed(y)?)))
                .filter(|(x, y)| {
                    nodes
                        .get(*y)
                        .and_then(|v| v.get(*x))
                        .map_or(u16::MAX, |n| n.used)
                        < 100
                })
                .map(|p| (p, 1))
                .collect_vec()
        },
        |e| e.0.abs_diff(emp_final.0) + e.1.abs_diff(emp_final.1),
        |e| *e == emp_final,
    )?;
    // Takes 5 steps to shift empty + goal state left by 1, then add 1 to shift goal data into
    // empty node at 0, 0
    Some(path_to_near_top_right.1 + 1 + (5 * (goal_init.0 - 1)))
}

pub mod parser {
    use super::Node;
    use nom::{
        bytes::complete::tag,
        character::complete::{multispace0, multispace1, u16 as nomu16, u8 as nomu8},
        combinator::{all_consuming, map},
        error::ParseError,
        multi::separated_list1,
        sequence::{delimited, preceded, separated_pair, terminated, tuple},
        Finish, IResult, Parser,
    };

    fn node_id(input: &str) -> IResult<&str, (u8, u8)> {
        separated_pair(preceded(tag("/dev/grid/node-x"), nomu8), tag("-y"), nomu8)(input)
    }

    fn node_stats(input: &str) -> IResult<&str, (u16, u16, u16, u8)> {
        fn padded_tera<'a, T, E: ParseError<&'a str>>(
            p: impl Parser<&'a str, T, E>,
        ) -> impl Parser<&'a str, T, E> {
            delimited(multispace0, terminated(p, tag("T")), multispace0)
        }
        let data_size = || padded_tera(nomu16);

        tuple((
            data_size(),
            data_size(),
            data_size(),
            terminated(nomu8, tag("%")),
        ))
        .parse(input)
    }

    fn node(input: &str) -> IResult<&str, Node> {
        fn make_node(
            (x, y): (u8, u8),
            (size, used, avail, use_percent): (u16, u16, u16, u8),
        ) -> Node {
            Node {
                x,
                y,
                size,
                used,
                avail,
                use_percent,
            }
        }
        map(tuple((node_id, node_stats)), |(id, stat)| {
            make_node(id, stat)
        })(input)
    }

    pub fn data(input: &str) -> Result<Vec<Node>, nom::error::Error<&str>> {
        fn strip_header(input: &str) -> IResult<&str, ()> {
            let (input, _) = tag("root@ebhq-gridcenter# df -h")(input)?;
            let (input, _) = multispace0(input)?;
            let (input, _) = tag("Filesystem              Size  Used  Avail  Use%")(input)?;
            Ok((input.trim(), ()))
        }

        all_consuming(preceded(strip_header, separated_list1(multispace1, node)))(input)
            .finish()
            .map(|(_, v)| v)
    }
}
