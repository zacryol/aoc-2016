use day06::*;
use std::fs;

fn main() {
    let input = fs::read_to_string("inputs/day06.txt").unwrap();
    let input = input.lines().filter(|l| !l.is_empty()).collect::<Vec<_>>();
    println!("Part 1: {}", error_correct(&input).unwrap());
    println!("Part 2: {}", error_correct_2(&input).unwrap());
}
