use std::{collections::HashMap, hash::Hash};

use rayon::prelude::*;

fn merge<T: Hash + Eq + Copy>(
    mut hm1: HashMap<T, usize>,
    hm2: HashMap<T, usize>,
) -> HashMap<T, usize> {
    hm2.into_iter()
        .for_each(|(k, v)| *hm1.entry(k).or_insert(0) += v);
    hm1
}

fn get_column_counts(input: &[&str]) -> Vec<HashMap<u8, usize>> {
    input
        .iter()
        .map(|s| {
            s.chars()
                .map(|c| HashMap::from([(c as u8, 1)]))
                .collect::<Vec<_>>()
        })
        .collect::<Vec<_>>()
        .into_par_iter()
        .reduce(
            || vec![HashMap::new(); 8],
            |v1, v2| {
                v1.into_iter()
                    .zip(v2.into_iter())
                    .map(|(hm1, hm2)| merge(hm1, hm2))
                    .collect()
            },
        )
}

pub fn error_correct(input: &[&str]) -> Option<String> {
    get_column_counts(input)
        .into_iter()
        .map(|col| {
            col.into_iter()
                .max_by_key(|(_, ct)| *ct)
                .map(|(c, _)| c as char)
        })
        .collect()
}

pub fn error_correct_2(input: &[&str]) -> Option<String> {
    get_column_counts(input)
        .into_iter()
        .map(|col| {
            col.into_iter()
                .min_by_key(|(_, ct)| *ct)
                .map(|(c, _)| c as char)
        })
        .collect()
}
